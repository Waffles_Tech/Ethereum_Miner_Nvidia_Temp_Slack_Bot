from time import sleep
import requests
import subprocess
import json

while True:
    gpu_temp_subprocess = subprocess.check_output("nvidia-smi -q -d temperature | grep Current", shell=True)
	gpu_temp_split = gpu_temp_subprocess.split(' ')
	gputempvar = gpu_temp_split[23] + "\xc2\xb0" + "C"

	cpu_temp_subprocess = subprocess.check_output("sensors | grep temp1", shell=True)
	cpu_temp_split = cpu_temp_subprocess.split(' ')
	cputempvar = cpu_temp_split[8].split("+")
	cputempvar1 = cputempvar[1].split(".")
	cputempfinal = cputempvar1[0] + "\xc2\xb0" + "C"

	payload = {"channel": "#mining", "username": "temp-bot", "text": "GPU Temp: " + gputempvar + "\nCPU Temp: " + cputempfinal, "icon_emoji": ":hammer_and_pick:"}
	posturl = '[SLACK-HOOK-URL]'
	send = requests.post(posturl, json=payload)

	sleep(36000)